USE [impresario]
GO

ALTER USER [MOS.ORG\apizzolato] WITH DEFAULT_SCHEMA=[dbo]
GO

ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\apizzolato]
GO
