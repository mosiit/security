-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\lthornton
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\lthornton] FOR LOGIN [MOS.ORG\lthornton]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\lthornton] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\lthornton]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\lthornton
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\lthornton] FOR LOGIN [MOS.ORG\lthornton]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\lthornton] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\lthornton]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\mrai
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\mrai] FOR LOGIN [MOS.ORG\mrai]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\mrai] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mrai]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\mrai
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\mrai] FOR LOGIN [MOS.ORG\mrai]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\mrai] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mrai]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\jstahlhacke
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\jstahlhacke] FOR LOGIN [MOS.ORG\jstahlhacke]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\jstahlhacke] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\jstahlhacke]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\jstahlhacke
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\jstahlhacke] FOR LOGIN [MOS.ORG\jstahlhacke]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\jstahlhacke] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\jstahlhacke]
GO
