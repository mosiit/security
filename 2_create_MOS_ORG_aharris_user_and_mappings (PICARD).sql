-- ***********************************************************
-- Mappings for:  Admits_Data
-- ***********************************************************

USE [Admits_Data]
GO
CREATE USER [MOS.ORG\aharris] FOR LOGIN [MOS.ORG\aharris]
GO
USE [Admits_Data]
GO
ALTER USER [MOS.ORG\aharris] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [Admits_Data]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aharris]
GO

-- ***********************************************************
-- Mappings for:  Conversion
-- ***********************************************************

USE [Conversion]
GO
CREATE USER [MOS.ORG\aharris] FOR LOGIN [MOS.ORG\aharris]
GO
USE [Conversion]
GO
ALTER USER [MOS.ORG\aharris] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [Conversion]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aharris]
GO

-- ***********************************************************
-- Mappings for:  impresario
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\aharris] FOR LOGIN [MOS.ORG\aharris]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\aharris] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aharris]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\aharris] FOR LOGIN [MOS.ORG\aharris]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\aharris] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aharris]
GO

-- ***********************************************************
-- Mappings for:  impresario_copy
-- ***********************************************************

USE [impresario_copy]
GO
CREATE USER [MOS.ORG\aharris] FOR LOGIN [MOS.ORG\aharris]
GO
USE [impresario_copy]
GO
ALTER USER [MOS.ORG\aharris] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_copy]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aharris]
GO

-- ***********************************************************
-- Mappings for:  Mil_Conv
-- ***********************************************************

USE [Mil_Conv]
GO
CREATE USER [MOS.ORG\aharris] FOR LOGIN [MOS.ORG\aharris]
GO
USE [Mil_Conv]
GO
ALTER USER [MOS.ORG\aharris] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [Mil_Conv]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aharris]
GO

-- ***********************************************************
-- Mappings for:  Mil_Data
-- ***********************************************************

USE [Mil_Data]
GO
CREATE USER [MOS.ORG\aharris] FOR LOGIN [MOS.ORG\aharris]
GO
USE [Mil_Data]
GO
ALTER USER [MOS.ORG\aharris] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [Mil_Data]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\aharris]
GO
USE [Mil_Data]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MOS.ORG\aharris]
GO
USE [Mil_Data]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [MOS.ORG\aharris]
GO
USE [Mil_Data]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aharris]
GO

-- ***********************************************************
-- Mappings for:  ProximityOne
-- ***********************************************************

USE [ProximityOne]
GO
CREATE USER [MOS.ORG\aharris] FOR LOGIN [MOS.ORG\aharris]
GO
USE [ProximityOne]
GO
ALTER USER [MOS.ORG\aharris] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [ProximityOne]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aharris]
GO
