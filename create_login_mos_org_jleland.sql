USE [master]
GO
CREATE LOGIN [MOS.ORG\jleland] FROM WINDOWS WITH DEFAULT_DATABASE=[impresario], DEFAULT_LANGUAGE=[us_english]
GO
USE [impresario]
GO
CREATE USER [MOS.ORG\jleland] FOR LOGIN [MOS.ORG\jleland]
GO
USE [impresario]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MOS.ORG\jleland]
GO
USE [impresario_cci]
GO
CREATE USER [MOS.ORG\jleland] FOR LOGIN [MOS.ORG\jleland]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MOS.ORG\jleland]
GO
USE [msdb]
GO
CREATE USER [MOS.ORG\jleland] FOR LOGIN [MOS.ORG\jleland]
GO
USE [msdb]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MOS.ORG\jleland]
GO
USE [ReportServer]
GO
CREATE USER [MOS.ORG\jleland] FOR LOGIN [MOS.ORG\jleland]
GO
USE [ReportServer]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MOS.ORG\jleland]
GO
