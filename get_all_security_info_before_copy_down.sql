/****************************************************************
Adopted from:  http://www.sqlservercentral.com/scripts/Logins/104030/

This Script Generates A script to Create all Logins, Server Roles
, DB Users and DB roles on a SQL Server

Greg Ryan

10/31/2013
****************************************************************/
SET NOCOUNT ON

DECLARE @sql NVARCHAR(MAX),
        @Line INT = 1,
        @max INT = 0,
        @@CurDB NVARCHAR(100) = ''

CREATE TABLE #logins (
        Idx INT IDENTITY,
        xSQL NVARCHAR(MAX)
       )

INSERT  INTO #logins
        (
         xSQL
        )
        SELECT  'IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N''' + sp.[name] + ''')
		' 
		+ '	CREATE LOGIN ' + QUOTENAME(sp.[name]) + ' FROM WINDOWS WITH DEFAULT_DATABASE=[' + sp.[default_database_name] + '], DEFAULT_LANGUAGE=[' + sp.[default_language_name] + ']
		'
		+ 'GO

		'
        FROM	sys.server_principals sp
		WHERE	sp.[name] LIKE '%MOS.ORG%'

PRINT '--SERVER NAME: ' + UPPER(@@SERVERNAME)
PRINT '--' + FORMAT(GETDATE(), 'MMM dd, yyyy HH:mm:ss')
PRINT ''
PRINT '/*****************************************************************************************/'
PRINT '/*************************************** Create Logins ***********************************/'
PRINT '/*****************************************************************************************/'
PRINT ' '
SELECT  xSQL AS [--]
FROM    #logins
DROP TABLE #logins

CREATE TABLE #roles (
        Idx INT IDENTITY,
        xSQL NVARCHAR(MAX)
       )

INSERT  INTO #roles
        (
         xSQL
        )
        SELECT  'EXEC sp_addsrvrolemember ' + QUOTENAME(L.name) + ', ' + QUOTENAME(R.name) + '
GO

'
        FROM    sys.server_principals L
        JOIN    sys.server_role_members RM
        ON      L.principal_id = RM.member_principal_id
        JOIN    sys.server_principals R
        ON      RM.role_principal_id = R.principal_id
        WHERE   L.[name] LIKE '%MOS.ORG%'


PRINT '/*****************************************************************************************/'
PRINT '/******************************Add Server Role Members     *******************************/'
PRINT '/*****************************************************************************************/'
PRINT ' '
SELECT  xSQL AS [--]
FROM    #roles
DROP TABLE #roles



PRINT '/*****************************************************************************************/'
PRINT '/*****************Add User and Roles membership to Indivdual Databases********************/'
PRINT '/*****************************************************************************************/'

CREATE TABLE #Db
       (
        idx int IDENTITY
       ,DBName nvarchar(100)
       );



INSERT INTO #Db
        SELECT
                name
            FROM
                master.dbo.sysdatabases
            WHERE
                name NOT IN ( 'Master' , 'Model' , 'msdb' , 'tempdb' )
            ORDER BY
                name;


SELECT
        @Max = MAX(idx)
    FROM
        #Db
SET @line = 1
--Select * from #Db


--Exec sp_executesql @SQL

WHILE @line <= @Max
      BEGIN
            SELECT
                    @@CurDB = DBName
                FROM
                    #Db
                WHERE
                    idx = @line

            SET @SQL = 'Use ' + @@CurDB + '

Declare  @@Script NVarChar(4000) = ''''
DECLARE cur CURSOR FOR

Select  ''Use ' + @@CurDB + ';
Go
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'''''' +
                mp.[name] + '''''')
CREATE USER ['' + mp.[name] + ''] FOR LOGIN ['' +mp.[name] + ''] WITH DEFAULT_SCHEMA=[dbo]; ''+ CHAR(13)+CHAR(10) +
''GO'' + CHAR(13)+CHAR(10) +

''EXEC sp_addrolemember N'''''' + rp.name + '''''', N'''''' + mp.[name] + ''''''; 
Go''  
FROM sys.database_role_members a
INNER JOIN sys.database_principals rp ON rp.principal_id = a.role_principal_id
INNER JOIN sys.database_principals AS mp ON mp.principal_id = a.member_principal_id
WHERE mp.[name] LIKE ''%MOS.ORG%''

OPEN cur

FETCH NEXT FROM cur INTO @@Script;
WHILE @@FETCH_STATUS = 0
BEGIN   
PRINT @@Script
FETCH NEXT FROM cur INTO @@Script;
END

CLOSE cur;
DEALLOCATE cur;';
--Print @SQL
Exec sp_executesql @SQL;
--Set @@Script = ''
            SET @Line = @Line + 1

      END

DROP TABLE #Db