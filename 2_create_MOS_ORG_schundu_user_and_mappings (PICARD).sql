-- ***********************************************************
-- Mappings for:  Conversion
-- ***********************************************************

USE [Conversion]
GO
CREATE USER [MOS.ORG\schundu] FOR LOGIN [MOS.ORG\schundu]
GO
USE [Conversion]
GO
ALTER USER [MOS.ORG\schundu] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [Conversion]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\schundu]
GO

-- ***********************************************************
-- Mappings for:  impresario
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\schundu] FOR LOGIN [MOS.ORG\schundu]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\schundu] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\schundu]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\schundu] FOR LOGIN [MOS.ORG\schundu]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\schundu] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\schundu]
GO

-- ***********************************************************
-- Mappings for:  Mil_Conv
-- ***********************************************************

USE [Mil_Conv]
GO
CREATE USER [MOS.ORG\schundu] FOR LOGIN [MOS.ORG\schundu]
GO
USE [Mil_Conv]
GO
ALTER USER [MOS.ORG\schundu] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [Mil_Conv]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\schundu]
GO

-- ***********************************************************
-- Mappings for:  Mil_Data
-- ***********************************************************

USE [Mil_Data]
GO
CREATE USER [MOS.ORG\schundu] FOR LOGIN [MOS.ORG\schundu]
GO
USE [Mil_Data]
GO
ALTER USER [MOS.ORG\schundu] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [Mil_Data]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\schundu]
GO
USE [Mil_Data]
GO
ALTER ROLE [db_datareader] ADD MEMBER [MOS.ORG\schundu]
GO
USE [Mil_Data]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [MOS.ORG\schundu]
GO
USE [Mil_Data]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\schundu]
GO
