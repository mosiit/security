DECLARE @tblSPList TABLE (SPID INT IDENTITY(1, 1)
                                   NOT NULL
                                   PRIMARY KEY CLUSTERED,
                          SPName SYSNAME)

DECLARE @SPName SYSNAME,
        @RowCounter INT,
        @RecordCounter INT,
        @UserName VARCHAR(100),
        @ExecuteSQL VARCHAR(1000)

SET @UserName = 'MOS.ORG\lthornton'

INSERT  INTO @tblSPList
        (SPName
        )
        SELECT  '[' + ROUTINE_SCHEMA + '].[' + ROUTINE_NAME + ']'
        FROM    INFORMATION_SCHEMA.ROUTINES
        WHERE   ROUTINE_TYPE = 'PROCEDURE'
		AND		ROUTINE_NAME LIKE 'L%'

--select 'debug', * from @tblSPList

SET @RecordCounter = (SELECT    COUNT(*)
                      FROM      @tblSPList)
SET @RowCounter = 1

WHILE (@RowCounter < @RecordCounter + 1) 
      BEGIN
            SELECT  @SPName = SPName
            FROM    @tblSPList
            WHERE   SPID = @RowCounter

            SET @ExecuteSQL = N'Grant VIEW Definition on ' + RTRIM(CAST(@SPName AS VARCHAR(128))) + ' to [' + @UserName + ']'

--commenting following EXEC statement so that
--one can verify before execute
--EXEC(@ExecuteSQL)

--Print Execute Statement
            PRINT @ExecuteSQL

            SET @RowCounter += 1
      END
GO