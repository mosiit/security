-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\lthornton
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\lthornton] FOR LOGIN [MOS.ORG\lthornton]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\lthornton] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\lthornton]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\lthornton
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\lthornton] FOR LOGIN [MOS.ORG\lthornton]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\lthornton] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\lthornton]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\mrai
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\mrai] FOR LOGIN [MOS.ORG\mrai]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\mrai] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mrai]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\mrai
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\mrai] FOR LOGIN [MOS.ORG\mrai]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\mrai] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mrai]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\jstahlhacke
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\jstahlhacke] FOR LOGIN [MOS.ORG\jstahlhacke]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\jstahlhacke] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\jstahlhacke]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\jstahlhacke
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\jstahlhacke] FOR LOGIN [MOS.ORG\jstahlhacke]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\jstahlhacke] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\jstahlhacke]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\mperry
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\mperry] FOR LOGIN [MOS.ORG\mperry]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\mperry] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mperry]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\mperry]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\mperry
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\mperry] FOR LOGIN [MOS.ORG\mperry]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\mperry] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mperry]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\mperry]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\bgraham
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\bgraham] FOR LOGIN [MOS.ORG\bgraham]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\bgraham] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\bgraham]
GO
USE [impresario]
GO
EXEC sp_addrolemember N'db_owner', N'MOS.ORG\bgraham'
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\bgraham
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\bgraham] FOR LOGIN [MOS.ORG\bgraham]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\bgraham] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\bgraham]
GO
USE [impresario_cci]
GO
EXEC sp_addrolemember N'db_owner', N'MOS.ORG\bgraham'
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\jsmillie
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\jsmillie] FOR LOGIN [MOS.ORG\jsmillie]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\jsmillie] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\jsmillie]
GO
USE [impresario]
GO
EXEC sp_addrolemember N'db_owner', N'MOS.ORG\jsmillie'
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\jsmillie
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\jsmillie] FOR LOGIN [MOS.ORG\jsmillie]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\jsmillie] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\jsmillie]
GO
USE [impresario_cci]
GO
EXEC sp_addrolemember N'db_owner', N'MOS.ORG\jsmillie'
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\agallant
-- ***********************************************************
--server roles
EXEC sp_addsrvrolemember 'MOS.ORG\agallant', 'securityadmin';  
GO  

-- database roles
USE [impresario]
GO
CREATE USER [MOS.ORG\agallant] FOR LOGIN [MOS.ORG\agallant]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\agallant] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\agallant]
GO
USE [impresario]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\agallant]
GO
USE [impresario]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [mos.org\agallant]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\agallant
-- ***********************************************************

-- database roles
USE [impresario_cci]
GO
CREATE USER [MOS.ORG\agallant] FOR LOGIN [MOS.ORG\agallant]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\agallant] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\agallant]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\agallant]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [mos.org\agallant]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\apetroff
-- ***********************************************************
--server roles
EXEC sp_addsrvrolemember 'MOS.ORG\apetroff', 'securityadmin';  
GO  

-- database roles
USE [impresario]
GO
CREATE USER [MOS.ORG\apetroff] FOR LOGIN [MOS.ORG\apetroff]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\apetroff] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\apetroff]
GO
USE [impresario]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\apetroff]
GO
USE [impresario]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [mos.org\apetroff]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\apetroff
-- ***********************************************************

-- database roles
USE [impresario_cci]
GO
CREATE USER [MOS.ORG\apetroff] FOR LOGIN [MOS.ORG\apetroff]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\apetroff] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\apetroff]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\apetroff]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [mos.org\apetroff]
GO

-- H. Sheridan, 4/17/2018 
--		Add Cristina Castilla (WO 92093 - Tessitura Tech Services SQL logins not working)
--		
-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\ccastilla 
-- ***********************************************************
USE [master]
GO
-- select [name] from sys.server_principals order by name
CREATE LOGIN [MOS.ORG\ccastilla] FROM WINDOWS WITH DEFAULT_DATABASE=[impresario], DEFAULT_LANGUAGE=[us_english]
GO
USE [impresario]
GO
CREATE USER [MOS.ORG\ccastilla] FOR LOGIN [MOS.ORG\ccastilla]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\ccastilla] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\ccastilla]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\ccastilla]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\ccastilla 
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\ccastilla] FOR LOGIN [MOS.ORG\ccastilla]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\ccastilla] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\ccastilla]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\ccastilla]
GO