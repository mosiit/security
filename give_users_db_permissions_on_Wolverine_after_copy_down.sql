-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\jballinger
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\jballinger] FOR LOGIN [MOS.ORG\jballinger]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\jballinger] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\jballinger]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\jballinger
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\jballinger] FOR LOGIN [MOS.ORG\jballinger]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\jballinger] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\jballinger]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\cemig
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\cemig] FOR LOGIN [MOS.ORG\cemig]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\cemig] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\cemig]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\cemig
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\cemig] FOR LOGIN [MOS.ORG\cemig]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\cemig] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\cemig]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\thoffmann
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\thoffmann] FOR LOGIN [MOS.ORG\thoffmann]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\thoffmann] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\thoffmann]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\thoffmann
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\thoffmann] FOR LOGIN [MOS.ORG\thoffmann]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\thoffmann] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\thoffmann]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\aileen 
-- NOTE: Aileen has additional permissions that other Tessitura TECH folks do not. Be careful when copying. 
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\aileen] FOR LOGIN [MOS.ORG\aileen]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\aileen] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aileen]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\aileen]
GO
USE [impresario]
GO

ALTER ROLE [db_securityadmin] ADD MEMBER [MOS.ORG\aileen]
GO
USE [impresario]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\aileen]

ALTER ROLE [db_securityadmin] ADD MEMBER [mos.org\aileen]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\aileen
-- NOTE: Aileen has additional permissions that other Tessitura TECH folks do not. Be careful when copying. 
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\aileen] FOR LOGIN [MOS.ORG\aileen]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\aileen] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\aileen]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\aileen]
GO

USE [impresario]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [MOS.ORG\aileen]
GO
USE [impresario]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\aileen]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\apetroff 
-- NOTE: Annie has additional permissions that other Tessitura TECH folks do not. Be careful when copying. 
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\apetroff] FOR LOGIN [MOS.ORG\apetroff]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\apetroff] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\apetroff]
GO
USE [impresario]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [MOS.ORG\apetroff]
GO
USE [impresario]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\apetroff]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\apetroff
-- NOTE: Annie has additional permissions that other Tessitura TECH folks do not. Be careful when copying. 
USE [impresario_cci]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [mos.org\aileen]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\lthornton
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\lthornton] FOR LOGIN [MOS.ORG\lthornton]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\lthornton] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\lthornton]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\lthornton
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\lthornton] FOR LOGIN [MOS.ORG\lthornton]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\lthornton] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\lthornton]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\mrai
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\mrai] FOR LOGIN [MOS.ORG\mrai]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\mrai] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mrai]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\mrai
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\apetroff] FOR LOGIN [MOS.ORG\apetroff]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\apetroff] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\apetroff]
GO
USE [impresario]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [MOS.ORG\apetroff]
GO
USE [impresario]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [MOS.ORG\apetroff]
GO

CREATE USER [MOS.ORG\mrai] FOR LOGIN [MOS.ORG\mrai]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\mrai] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mrai]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\jstahlhacke
-- ***********************************************************

USE [impresario]
GO
CREATE USER [MOS.ORG\jstahlhacke] FOR LOGIN [MOS.ORG\jstahlhacke]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\jstahlhacke] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\jstahlhacke]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\jstahlhacke
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\jstahlhacke] FOR LOGIN [MOS.ORG\jstahlhacke]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\jstahlhacke] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\jstahlhacke]
GO

-- H. Sheridan, 4/17/2018 
--		Add Cristina Castilla and Maris Perry (WO 92093 - Tessitura Tech Services SQL logins not working)
--		
-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\ccastilla 
-- ***********************************************************
USE [master]
GO
-- select [name] from sys.server_principals order by name
CREATE LOGIN [MOS.ORG\ccastilla] FROM WINDOWS WITH DEFAULT_DATABASE=[impresario], DEFAULT_LANGUAGE=[us_english]
GO
USE [impresario]
GO
CREATE USER [MOS.ORG\ccastilla] FOR LOGIN [MOS.ORG\ccastilla]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\ccastilla] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\ccastilla]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\ccastilla]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\ccastilla 
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\ccastilla] FOR LOGIN [MOS.ORG\ccastilla]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\ccastilla] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\ccastilla]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\ccastilla]
GO

-- ***********************************************************
-- Mappings for:  impresario - MOS.ORG\mperry 
-- ***********************************************************
USE [master]
GO
CREATE LOGIN [MOS.ORG\mperry] FROM WINDOWS WITH DEFAULT_DATABASE=[impresario], DEFAULT_LANGUAGE=[us_english]
GO
USE [impresario]
GO
CREATE USER [MOS.ORG\mperry] FOR LOGIN [MOS.ORG\mperry]
GO
USE [impresario]
GO
ALTER USER [MOS.ORG\mperry] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mperry]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\mperry]
GO

-- ***********************************************************
-- Mappings for:  impresario_cci - MOS.ORG\mperry
-- ***********************************************************

USE [impresario_cci]
GO
CREATE USER [MOS.ORG\mperry] FOR LOGIN [MOS.ORG\mperry]
GO
USE [impresario_cci]
GO
ALTER USER [MOS.ORG\mperry] WITH DEFAULT_SCHEMA=[dbo]
GO
USE [impresario_cci]
GO
ALTER ROLE [ImpUsers] ADD MEMBER [MOS.ORG\mperry]
GO
ALTER ROLE [db_owner] ADD MEMBER [MOS.ORG\mperry]
GO


